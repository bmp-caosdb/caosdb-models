"""
This script provides the a function to read a DataModel from a yaml file.

If a file name is passed to parse_model_from_yaml it is parsed and a DataModel
is created. The yaml file needs to be structured in a certain way which will be
described in the following.

The file should only contain a dictionary. The keys are the names of
RecordTypes or Properties. The values are again dictionaries describing the
entities. This information can be defined via the keys listed in KEYWORDS.
Notably, properties can be given in a dictionary under the xxxx_properties keys
and will be added with the respective importance. These properties can be
RecordTypes or Properties and can be defined right there.
Every Property or RecordType only needs to be defined once anywhere. When it is
not defined, simply the name can be supplied with no value.
Parents can be provided under the inherit_from_xxxx keywords. The value needs
to be a list with the names. Here, NO NEW entities can be defined.
"""
import re

import caosdb as db
import yaml

from .data_model import DataModel

KEYWORDS = ["parent",
            "importance",
            "datatype",
            "unit",
            "description",
            "recommended_properties",
            "obligatory_properties",
            "suggested_properties",
            "inherit_from_recommended",
            "inherit_from_suggested",
            "inherit_from_obligatory", ]


class TwiceDefinedException(Exception):
    def __init__(self, name):
        super().__init__("The Entity '{}' was defined multiple times!".format(
            name))


def parse_model_from_yaml(filename):
    parser = Parser()

    return parser.parse_model_from_yaml(filename)


class Parser(object):
    def __init__(self):
        self.model = {}
        self.treated = []

    def parse_model_from_yaml(self, filename):
        with open(filename, 'r') as outfile:
            ymlmodel = yaml.load(outfile)

        if not isinstance(ymlmodel, dict):
            raise ValueError("Yaml file should only contain one dictionary!")

        # add all names to ymlmodel; initialize properties

        for name, entity in ymlmodel.items():
            self._add_entity_to_model(name, entity)
        # initialize recordtypes
        self._set_recordtypes()

        for name, entity in ymlmodel.items():
            self._treat_entity(name, entity)

        return DataModel(self.model.values())

    def _get_key(self, dictionary, key):
        if key in dictionary:
            return dictionary[key]
        else:
            return None

    def _add_entity_to_model(self, name, definition):
        """ adds names of Properties and RecordTypes to the model dictionary

        Properties are also initialized.
        """

        if name not in self.model:
            self.model[name] = None

        if definition is None:
            return

        if (self.model[name] is None
                and isinstance(definition, dict)
                # is it a property
                and "datatype" in definition
                # but not a list
                and not definition["datatype"].startswith("LIST")):

            # get the datatype
            try:
                dt = db.__getattribute__(definition["datatype"])
            except AttributeError:
                raise ValueError("Unknown Datatype.")
            # and create the new property
            self.model[name] = db.Property(name=name, datatype=dt)

        # add other definitions recursively

        for prop_type in ["recommended_properties",
                          "suggested_properties", "obligatory_properties"]:

            if prop_type in definition:

                for n, e in definition[prop_type].items():
                    self._add_entity_to_model(n, e)

    def _add_to_recordtype(self, ent_name, props, importance):
        for n, e in props.items():
            if n in KEYWORDS:
                continue

            if isinstance(e, dict) and "datatype" in e and e["datatype"].startswith("LIST"):
                match = re.match(r"LIST[(](.*)[)]", e["datatype"])

                if match is None:
                    raise ValueError("List datatype definition is wrong")
                dt = db.LIST(match.group(1))
                self.model[ent_name].add_property(name=n,
                                                  importance=importance,
                                                  datatype=dt
                                                  )
            else:
                self.model[ent_name].add_property(name=n,
                                                  importance=importance)

    def _inherit(self, name, prop, inheritance):
        if not isinstance(prop, list):
            raise Exception("parents in list")

        for pname in prop:
            if not isinstance(pname, str):
                raise ValueError("Only provide the names of parents.")
            self.model[name].add_parent(name=pname, inheritance=inheritance)

    def _treat_entity(self, name, definition):
        """ parse the definition and the information to the entity """

        try:
            if definition is None:
                return

            if ("datatype" in definition
                    and definition["datatype"].startswith("LIST")):

                return

            if name in self.treated:
                raise TwiceDefinedException(name)

            for prop_name, prop in definition.items():
                if prop_name == "unit":
                    self.model[name].unit = prop

                elif prop_name == "description":
                    self.model[name].description = prop

                elif prop_name == "recommended_properties":
                    self._add_to_recordtype(name, prop, importance=db.RECOMMENDED)

                    for n, e in prop.items():
                        self._treat_entity(n, e)

                elif prop_name == "obligatory_properties":
                    self._add_to_recordtype(name, prop, importance=db.OBLIGATORY)

                    for n, e in prop.items():
                        self._treat_entity(n, e)

                elif prop_name == "suggested_properties":
                    self._add_to_recordtype(name, prop, importance=db.SUGGESTED)

                    for n, e in prop.items():
                        self._treat_entity(n, e)

                # datatype is already set
                elif prop_name == "datatype":
                    continue

                elif prop_name == "inherit_from_obligatory":
                    self._inherit(name, prop, db.OBLIGATORY)
                elif prop_name == "inherit_from_recommended":
                    self._inherit(name, prop, db.RECOMMENDED)
                elif prop_name == "inherit_from_suggested":
                    self._inherit(name, prop, db.SUGGESTED)

                else:
                    raise ValueError("unvalid keyword: {}".format(prop_name))
        except Exception as e:
            print("Error in treating: "+name)
            raise e
        self.treated.append(name)

    def _set_recordtypes(self):
        """ properties are defined in first iteration; set remaining as RTs """

        for key, value in self.model.items():
            if value is None:
                self.model[key] = db.RecordType(name=key)


if __name__ == "__main__":
    model = parse_model_from_yaml('data_model.yml')
    print(model)

# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
'''template1.py

Define a data model (i.e. a collection of RecordTypes and Properties) for
CaosDB which is used to represent your data. This data model can be updated by
calling it again.
'''

import caosdb as db

from caosmodels.data_model import DataModel

""" Your data model. A dict for alias=>db.Entity.

A data model usually consists of RecordTypes and Properties. E.g.
RecordTypes `Experiment`, `Person`, and other *things* and Properties
`date`, `location`, and other properties of the things.

Add Properties to the RecordTypes with an appropriate `importance` to make
the server check the existence of these properties when inserting Records
later on.
"""
dm = DataModel([
    (db.RecordType(name="DummyRecordType1")
     .add_parent(name="DummyRecordParent1")
     .add_property(name="dummyProperty1")
     .add_property(name="Author",
                   importance=db.SUGGESTED)),
    db.RecordType(name="DummyRecordParent1"),
    db.Property(
        name="dummyProperty1",
        unit="cm",
        datatype=db.DOUBLE),
])

dm.sync_data_model()

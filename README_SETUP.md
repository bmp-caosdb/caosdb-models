# Installation
```
pip3 install . --user
pip3 install tox --user
# or
apt-get install tox
```

# Run Unit Tests
tox

# Code Formatting

autopep8 -i -r ./
